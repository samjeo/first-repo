{  
   "records":[  
      {  
         "title":"The Lion king Dual",
         "genre":"Animation, Adventure, Drama",
         "year":"2019",
         "videoOne":"1",
         "videoTwo":"http://dl2.jiocloud.link//The.Lion.King.2019.480p.NEW.HDCAM.Hindi-English.x264-KatmovieHD.it.mkv",
         "videoThree":"http://dl2.jiocloud.link//The.Lion.King.2019.480p.NEW.HDCAM.Hindi-English.x264-KatmovieHD.it.mkv",
         "info":"After the murder of his father, a young lion prince flees his kingdom only to learn the true meaning of responsibility and bravery.",
         "cover":"https://m.media-amazon.com/images/M/MV5BZGI2OWNiZTktODYyMy00MGY4LWFhYTEtNmIyMTFmYWU3NmFmXkEyXkFqcGdeQXVyODc0OTEyNDU@._V1_QL50_SY1000_CR0,0,711,1000_AL_.jpg"
      },
      {  
         "title":"Fast & Furious Presents: Hobbs & Shaw Dual",
         "genre":"Action, Adventure",
         "year":"2019",
         "videoOne":"1",
         "videoTwo":"http://dl2.jiocloud.link/CAM/Hobbs.&.Shaw.2019.480p.HDCAM.Hin-Eng.x264-KatmovieHD.Eu.mkv",
         "videoThree":"http://dl2.jiocloud.link/CAM/Hobbs.&.Shaw.2019.480p.HDCAM.Hin-Eng.x264-KatmovieHD.Eu.mkv",
         "info":"Lawman Luke Hobbs and outcast Deckard Shaw form an unlikely alliance when a cyber-genetically enhanced villain threatens the future of humanity.",
         "cover":"https://m.media-amazon.com/images/M/MV5BZTUwZmUyMmMtYmQxOC00NzRmLTk3ZTgtYzkyNmYzYzIxZGM5XkEyXkFqcGdeQXVyODk2ODI3MTU@._V1_QL50_SY1000_CR0,0,631,1000_AL_.jpg"
      },
      {  
         "title":"Avengers Endgame Dual",
         "genre":"Action",
         "year":"2019",
         "videoOne":"1",
         "videoTwo":"http://dl.jiocloud.link//Avengers.Endgame.2019.720p.HDRip.Hindi.(Clean)-English.x264-KatmovieHD.it.mkv",
         "videoThree":"http://dl.jiocloud.link//Avengers.Endgame.2019.720p.HDRip.Hindi.(Clean)-English.x264-KatmovieHD.it.mkv",
         "info":"After the devastating events of Avengers: Infinity War (2018), the universe is in ruins. With the help of remaining allies, the Avengers assemble once more in order to undo Thanos' actions and restore order to the universe.",
         "cover":"https://drive.google.com/uc?export=view&id=19jFACv_aFeK-8xpcHim86PeI6YnWn1m6"
      },
      {  
         "title":"Spider Man Far from Home Dual",
         "genre":"Action, Adventure, Sci-Fi",
         "year":"2019",
         "videoOne":"1",
         "videoTwo":"https://plus2.uhub.com/sapi/download/video?action=get&userid=6ae2855d4a846a0e7f466d661ff13b5e2268e17e45b46b527948d07792a42490accd4655d534e1f3&id=171961375&dk=MTVpYTVzc2sxd3h0Zy0xNzE5NjEzNzUubWt2&node=DS06&filename=sp.mkv",
         "videoThree":"https://plus2.uhub.com/sapi/download/video?action=get&userid=6ae2855d4a846a0e7f466d661ff13b5e2268e17e45b46b527948d07792a42490accd4655d534e1f3&id=171961375&dk=MTVpYTVzc2sxd3h0Zy0xNzE5NjEzNzUubWt2&node=DS06&filename=sp.mkv",
         "info":"Following the events of Avengers: Endgame, Spider-Man must step up to take on new threats in a world that has changed forever.",
         "cover":"http://t0.gstatic.com/images?q=tbn:ANd9GcRPzzfxN6Ugvq902Ut7A-QkCDOqLzzbiYQ2bPk2pn5D9fBZBrT_"
      },
      {  
         "title":"Khandaani Shafakhana",
         "genre":"Comedy, Drama",
         "year":"2019",
         "videoOne":"2",
         "videoTwo":"http://ssd7.polosolo.net/HDFriday/Bollywood-Movies/720p/Khandaani%20Shafakhana%202019%20pDvDRip%20HDFriday.mp4",
         "videoThree":"http://ssd7.polosolo.net/HDFriday/Bollywood-Movies/720p/Khandaani%20Shafakhana%202019%20pDvDRip%20HDFriday.mp4",
         "info":"A feisty girl from Punjab tries to continue her family traditions by opening a sex clinic to help and aware others in her hometown.",
         "cover":"https://m.media-amazon.com/images/M/MV5BYTY5NGZlY2QtNTQxYy00ZTQxLTgxMGUtNDA0MmIyZjA0NGFlXkEyXkFqcGdeQXVyMjUxMTY3ODM@._V1_QL50_SY1000_CR0,0,695,1000_AL_.jpg"
      },
      {  
         "title":"Arjun Patiala",
         "genre":"Action, Comedy, Romance",
         "year":"2019",
         "videoOne":"2",
         "videoTwo":"http://get-dl.com/jattwifi77/files/Bollywood_Movies/New_Bollywood_Movies/Arjun_Patiala_(2019)/Arjun_Patiala_RdxHD.Com-.mkv",
         "videoThree":"http://get-dl.com/jattwifi77/files/Bollywood_Movies/New_Bollywood_Movies/Arjun_Patiala_(2019)/Arjun_Patiala_RdxHD.Com-.mkv",
         "info":"This spoof comedy narrates the story of a cop Arjun Patiala (Diljit Dosanjh) and his sidekick Onidda Singh (Varun Sharma). Together, will they be able to accomplish their mission of a crime-free town with their goofy style of policing?",
         "cover":"https://m.media-amazon.com/images/M/MV5BNjZkNTA3ZDItNGFlYi00MTgwLThjYjgtOTZhODJjNDA2NjE5XkEyXkFqcGdeQXVyOTAzMTc2MjA@._V1_QL50_SY1000_CR0,0,691,1000_AL_.jpg"
      },
      {  
         "title":"Penalty",
         "genre":"Sport",
         "year":"2019",
         "videoOne":"2",
         "videoTwo":"http://get-dl.com/jattwifi77/files/Bollywood_Movies/New_Bollywood_Movies/Penalty_(2019)_SCRRip/Penalty_RdxHD.Com-.mkv",
         "videoThree":"http://get-dl.com/jattwifi77/files/Bollywood_Movies/New_Bollywood_Movies/Penalty_(2019)_SCRRip/Penalty_RdxHD.Com-.mkv",
         "info":"A story of a North-east Indian football player who's fighting day and night with the society in order to achieve his dreams. The film covers all kinds of layers in one single film from",
         "cover":"https://m.media-amazon.com/images/M/MV5BNGE1MTE5MTktOGQ0OS00MjAyLWEwYTItNzI5MDg5ZWUzMjRiXkEyXkFqcGdeQXVyMTA0NDE3Mzk0._V1_QL50_SY1000_CR0,0,709,1000_AL_.jpg"
      },
      {  
         "title":"Shadaa",
         "genre":"Comedy",
         "year":"2019",
         "videoOne":"2",
         "videoTwo":"http://get-dl.com/jattwifi77/files/Punjabi_Movies/New_Punjabi_Movies/Shadaa_(2019)_HDRip/Shadaa_RdxHD.CoM-.mkv",
         "videoThree":"http://get-dl.com/jattwifi77/files/Punjabi_Movies/New_Punjabi_Movies/Shadaa_(2019)_HDRip/Shadaa_RdxHD.CoM-.mkv",
         "info":"Amidst pressures from his parents to hurry up and wed, a young man seeks his perfect match. After many failed attempts with matchmakers, his parents are delighted when their son finally",
         "cover":"https://drive.google.com/uc?export=view&id=1M_F2snDWUV2NO6BcazmOIfDlVXVnA1hh"
      }
   ]
}